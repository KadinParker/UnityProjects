﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour {

    public float mouseSensitivity = 10;
    public Transform target;
    public float dstFromTarget = 2;

    float yaw;
    float pitch;

  // Update is called once per frame
  void Update () {

      yaw += Input.GetAxis("Mouse X") * mouseSensitivity;
      pitch -= Input.GetAxis("Mouse Y") * mouseSensitivity;

      Vector3 targetRotation = new Vector3(pitch, yaw);
      transform.eulerAngles = targetRotation;

      transform.position = target.position - transform.forward * dstFromTarget;
  }
}
